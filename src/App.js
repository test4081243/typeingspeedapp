import logo from './logo.svg';


import './App.css'; 
import TypingSpeedTest from './components/Tst'; 
  
function App() { 
  return ( 
    <div className="App"> 
      <TypingSpeedTest /> 
    </div> 
  ); 
} 
  
export default App; 