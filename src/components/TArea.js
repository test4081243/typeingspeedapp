
import React from 'react'; 

const TypingArea = ({ 
typingText, 
inpFieldValue, 
timeLeft, 
mistakes, 
WPM, 
CPM, 
initTyping, 
handleKeyDown, 
resetArea, 
}) => { 
return ( 
	<div className="section"> 
	<div className="section1"> 
		<p id="paragraph">{typingText}</p> 
	</div> 
	<div className="section2"> 
		<ul className="resultDetails"> 
		<li className="time"> 
			<p>Time Left:</p> 
			<span> 
			<b>{timeLeft}</b>s 
			</span> 
		</li> 
		<li className="mistake"> 
			<p>Mistakes:</p> 
			<span>{mistakes}</span> 
		</li> 
		<li className="wpm"> 
			<p>WPM(Word per minute):</p> 
			<span>{WPM}</span> 
		</li> 
		<li className="cpm"> 
			<p>CPM(Correct per minute):</p> 
			<span>{CPM}</span> 
		</li> 
		</ul> 
		<button onClick={resetArea} className="btn"> 
		Again Check 
		</button> 
	</div> 
	</div> 
); 
}; 

export default TypingArea; 
